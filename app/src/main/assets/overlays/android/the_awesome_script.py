#!/usr/bin/python
import shutil
import os
import subprocess

values=[['Greensea','ff16a085'],['Carrot','ffe67e22'],['Belizehole','ff246d9c'],['MidnightBlue','ff2c3e50'],['Alizarin','ffe74c3c'],['WetAsphalt','ff34495e'],['Pumpkin','ffd35400'],['Nephritis','ff27ae60'],['Wisteria','ff8e44ad'],['GrayHound','ff7f8c8d'],['Smoked','ff1e1e20'],['BlueSky','ff3498db']]

def work(directory,light,theFile,theFileMinusRes,fileFolder):
    #dirmaker
    if not os.path.exists('type2_'+directory+fileFolder):
        os.makedirs('type2_'+directory+fileFolder)
        shutil.copyfile(theFile, 'type2_'+directory+theFileMinusRes)   
    else:
        shutil.copyfile(theFile, 'type2_'+directory+theFileMinusRes)
    #replacer
    call1='s/#ff16a085/#'+light+'/g'
    subprocess.call(['sed','-i','.old',call1, 'type2_'+directory+theFileMinusRes])
    
    #remover
    subprocess.call(['rm','type2_'+directory+theFileMinusRes+'.old'])

proc=subprocess.Popen('ag -l "#ff16a085" --ignore=*.py', shell=True, stdout=subprocess.PIPE, )
output=proc.communicate()[0]
mainList = output.split('\n')
mainList = mainList[:-1]
mainList2 = []
mainList3 = []

for k in range(len(mainList)):
    mainList2.append(mainList[k][3:])
    temp = mainList2[k].split('/')
    temp=temp[:-1]
    temp='/'.join(temp)
    mainList3.append(temp)

for j in range(len(mainList)):
    print 'Replacing ' + mainList[j]
    for i in range(12):
        work(values[i][0],values[i][1],mainList[j],mainList2[j],mainList3[j])
